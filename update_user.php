<?php

if (isset($_POST['update'])) {
    echo $_POST['id'];

    $data = array(
        'firstName' => $_POST['firstname'],
        'lastName' => $_POST['lastname'],
        'email' => $_POST['email']
    );

    $db->where('id', $_POST['id']);
    
    if ($db->update('user', $data)) {
        echo $db->count . ' records were updated';
        header('Location: task.php');
        exit();
    } else
        echo 'update failed: ' . $db->getLastError();
}
