<?php

require_once('db_init.php')

?>

<form method="POST">
    <input type="text" name="firstname" placeholder="firstname" required>
    <input type="text" name="lastname" placeholder="lastname" required>
    <input type="email" name="email" placeholder="email" required>
    <button type="submit" name="add_user">Save</button>
</form>


<?php

if (isset($_POST['add_user'])) {
    $data = array(
        "email" => $_POST['email'],
        "firstName" =>  $_POST['firstname'],
        "lastName" => $_POST['lastname'],
    );
    $id = $db->insert('user', $data);
    if ($id) {
        header('Location: task.php');
        exit();
        echo 'user was created. Id=' . $id;
    } 
}

require_once('show_users.php');
