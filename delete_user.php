<?php

if (isset($_POST['delete'])) {
    $db->where('id', $_POST['id']);
    if ($db->delete('user')) {
        echo $db->count . ' records were deleted';
        header('Location: task.php');
        exit();
    } else
        echo 'delete failed: ' . $db->getLastError();
}
